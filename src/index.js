
const request = require('superagent');

const config = require('./config')

const baseUrl = config.get('marathon.url').replace(/(\/$)/, '')
const appId = config.get('app.id')
const appImage = config.get('app.image')

request.put(`${ baseUrl }/v2/apps/${ appId }`)
  .query({
    force: true,
    partialUpdate: true,
  })
  .send({
    id: appId,
    container: {
      docker: {
        image: appImage,
      },
    },
  })
  .type('json')
  .accept('json')
  .then(res => console.log(res.body))
  .catch(console.error)
