const fs = require( 'fs' )

const convict = require( 'convict' )

const config = convict( {
  marathon: {
    url: {
      default: 'http://leader.mesos:8080/',
      format: 'url',
      arg: 'marathonUrl',
      env: 'MARATHON_BASE_URL',
    },
  },
  app: {
    id:  {
      format: String,
      default: null,
      arg: 'appId',
      env: 'APP_ID',
    },
    image: {
      format: String,
      default: null,
      arg: 'appImage',
      env: 'APP_IMAGE',
    }
  },
} )

if ( fs.existsSync( './marathon.json' ) ) {
  config.loadFile( './marathon.json' )
}

config.validate( { allowed: 'strict' } )

module.exports = config
