FROM node:8-stretch as install-dev

COPY package.json .
COPY yarn.lock .
COPY .yarnclean .

RUN yarn install

FROM node:8-stretch as build

COPY --from=install-dev node_modules node_modules
COPY . .

RUN yarn package:docker

FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
  dumb-init \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

COPY --from=build dist/marathon-update /usr/local/bin/marathon-update

# Set the entrypoint to start with dumb-init to manage signal handling and threads
ENTRYPOINT ["dumb-init", "--"]
CMD ["/usr/local/bin/marathon-update"]
